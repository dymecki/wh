<?php

Route::get('/', 'GameController@index')->name('index');
Route::post('/', 'HomeController@userParameters');
//Route::get('/', 'HomeController@index');

Route::resource('games', 'GameController');
Route::resource('brands', 'BrandController');

//Route::resource('categories', 'CategoryController');
Route::get('categories/{id}', 'CategoryController@show')->name('categories.show');

Route::get('games-list', 'GameController@ajaxIndex');
