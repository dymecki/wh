<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\GameCountryBlock
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameCountryBlock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameCountryBlock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameCountryBlock query()
 */
	class GameCountryBlock extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\BrandGame
 *
 * @property int $id
 * @property int|null $brandid
 * @property string|null $launchcode
 * @property string|null $category
 * @property int|null $seq
 * @property int|null $hot
 * @property int|null $new
 * @property string|null $sub_category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereBrandid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereHot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereLaunchcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereSeq($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereSubCategory($value)
 */
	class BrandGame extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\GameProvider
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $distributor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider whereDistributor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider whereTitle($value)
 */
	class GameProvider extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Game
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game query()
 */
	class Game extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\GameBrandBlock
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameBrandBlock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameBrandBlock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameBrandBlock query()
 */
	class GameBrandBlock extends \Eloquent {}
}

