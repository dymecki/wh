<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateGamesViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = 'create view games_view as
select game.*, bg.*, country

from tests.brands b

join tests.brand_games bg		on bg.brand_id = b.brand_id
join tests.games g				on g.id = bg.game_id

left join tests.games_brand_block gbb		on gbb.brand_id = bg.brand_id and gbb.game_id = bg.game_id
left join tests.games_country_block gcb 	on gcb.brand_id = bg.brand_id and gcb.game_id = bg.game_id';

        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
