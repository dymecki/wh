<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Country;
use App\Models\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function index()
    {
//        dd(Game::with(['provider', 'type'])->take(20)->get()->first());
        return view('games.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Game $game
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        //
    }

    public function ajaxIndex()
    {
        return view('games._ajax')->with('games', Game::with(['provider', 'type'])->take(20)->get());
    }
}
