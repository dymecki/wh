<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function show($categoryName)
    {
        $category = Category::where('category', $categoryName)->first();

//        dd($category->games($categoryName)->first()->provider);

        return view('categories.show')
            ->with('category', $category)
            ->with('games', $category->games($categoryName));
    }
}
