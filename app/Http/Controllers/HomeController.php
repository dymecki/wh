<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('games.index')->with('games', Game::take(20));
    }

    public function userParameters(Request $request)
    {
        $request->session()->put('user.brand', $request->post('brand'));
        $request->session()->put('user.country', $request->post('country'));

        return redirect()->route('index');
    }
}
