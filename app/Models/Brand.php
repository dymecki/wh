<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Game;

class Brand extends Model
{
    protected $table = 'brands';

    public function games()
    {
        return $this->belongsToMany(Game::class, 'brand_games', 'brandid', 'launchcode');
    }

    public function filteredGames()
    {
        return \DB::table('brands')
                  ->select(['game.*', 'bg.*', 'country'])
                  ->join('brand_games as bg', 'brands.id', 'bg.brandid')
                  ->join('game', 'game.launchcode', 'bg.launchcode')
                  ->leftJoin('game_brand_block as gbb', static function ($join) {
                      $join->on('gbb.brandid', 'bg.brandid')->on('gbb.launchcode', 'bg.launchcode');
                  })
                  ->leftJoin('game_country_block as gcb', static function ($join) {
                      $join->on('gcb.brandid', 'bg.brandid')->on('gcb.launchcode', 'bg.launchcode');
                  })
                  ->whereNull('gbb.launchcode')
                  ->where(static function ($query) {
                      $query->where('country', '<>', session('user.country'))->orWhereNull('country');
                  })
                  ->take(20)
                  ->get();
    }
}
