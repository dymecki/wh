<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GameBrandBlock
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameBrandBlock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameBrandBlock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameBrandBlock query()
 * @mixin \Eloquent
 */
class GameBrandBlock extends Model
{
    //
}
