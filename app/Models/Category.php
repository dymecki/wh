<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    public function brands()
    {
        return $this->hasMany(Brand::class, 'id', 'brandid');
    }

    public function games($categorySlug)
    {
        return \DB::table('category')
//            return Category::
                  ->select(['game.*', 'bg.*', 'country'])
                  ->join('brands', 'category.brandid', 'brands.id')
                  ->join('brand_games as bg', 'brands.id', 'bg.brandid')
                  ->join('game', 'game.launchcode', 'bg.launchcode')
                  ->leftJoin('game_brand_block as gbb', static function ($join) {
                      $join->on('gbb.brandid', 'bg.brandid')->on('gbb.launchcode', 'bg.launchcode');
                  })
                  ->leftJoin('game_country_block as gcb', static function ($join) {
                      $join->on('gcb.brandid', 'bg.brandid')->on('gcb.launchcode', 'bg.launchcode');
                  })
                  ->whereNull('gbb.launchcode')
                  ->where('category.category', $categorySlug)
                  ->where(static function ($query) {
                      $query->where('country', '<>', session('user.country'))->orWhereNull('country');
                  })
                  ->take(20)
                  ->get();
    }
}
