<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Game
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $name
 * @property int|null $width
 * @property int|null $height
 * @property string $launchcode
 * @property int $active
 * @property string|null $image
 * @property string|null $last_modified
 * @property int $desktop
 * @property int $mobile
 * @property int|null $game_type_id
 * @property float|null $min
 * @property float|null $max
 * @property float|null $gamelimit
 * @property int|null $fun_supported
 * @property int|null $iframe
 * @property string|null $date_added
 * @property int|null $game_provider_id
 * @property float|null $rtp
 * @property int|null $jackpot
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereDesktop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereFunSupported($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereGameProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereGameTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereGamelimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereIframe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereJackpot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereLastModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereLaunchcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereRtp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereWidth($value)
 */
class Game extends Model
{
    protected $table = 'game';
//    protected $primaryKey = 'launchcode';

    public function provider()
    {
        return $this->hasOne(GameProvider::class, 'id', 'game_provider_id');
    }

    public function type()
    {
        return $this->hasOne(GameType::class, 'id', 'game_type_id');
    }
}
