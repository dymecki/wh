<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BrandGame
 *
 * @property int $id
 * @property int|null $brandid
 * @property string|null $launchcode
 * @property string|null $category
 * @property int|null $seq
 * @property int|null $hot
 * @property int|null $new
 * @property string|null $sub_category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereBrandid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereHot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereLaunchcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereSeq($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandGame whereSubCategory($value)
 * @mixin \Eloquent
 */
class BrandGame extends Model
{
    protected $table = 'brand_games';
}
