<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GameProvider
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $distributor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider whereDistributor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameProvider whereTitle($value)
 * @mixin \Eloquent
 */
class GameProvider extends Model
{
    protected $table = 'game_providers';

    public function games()
    {
        $this->hasMany(Game::class, 'id', 'game_provider_id');
    }
}
