<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GameCountryBlock
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameCountryBlock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameCountryBlock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GameCountryBlock query()
 * @mixin \Eloquent
 */
class GameCountryBlock extends Model
{
    //
}
