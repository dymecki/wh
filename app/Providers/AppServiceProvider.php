<?php

namespace App\Providers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Country;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \View::share('brands', Brand::all()->sortBy('brand'));
        \View::share('countries', Country::all());
        \View::share('categories', Category::groupBy(['name', 'category'])->get(['name', 'category']));
    }
}
