<div
    class="card h-250"
    style="width: 16.4rem ; margin: 15px 15px 0 0; float: left"
    data-toggle="popover"
{{--    data-trigger="focus"--}}
    data-content="Launchcode: {{ $game->launchcode }}"
>
    <img
        class="card-img-top"
        src="https://stage.whgstage.com/scontent/images/games/{{ $game->launchcode }}.jpg"
        alt="Card image cap"
    >

    <div class="card-body">
        <h5 class="card-title">{{ $game->name }}</h5>
{{--        <p class="card-text">--}}
{{--            Provider: {{ $game->provider->name }}--}}
{{--            Type: {{ $game->type->name }}--}}
{{--        </p>--}}
    </div>
    <ul class="list-group list-group-flush">
{{--        <li class="list-group-item">{{ $game->provider->name }}</li>--}}
{{--        <li class="list-group-item">{{ $game->type->name }}</li>--}}
    </ul>
</div>
