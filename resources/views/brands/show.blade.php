@extends('layout')

@section('content')

    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th scope="col">Image</th>
            <th scope="col">Game</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($brand->games as $game)
            <tr>
                <td><img src="https://stage.whgstage.com/scontent/images/games/{{ $game->launchcode }}.jpg"/></td>
                <td>{{ $game->name }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection
