<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.8/angular.min.js"></script>--}}

    <style>
        .content .card:hover {
            border-color: #2fa360;
        }
    </style>
    <script>
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });

        $(document).on("click", '.card', function (event) {
            $('[data-toggle="popover"]').popover();
        });
    </script>
</head>
<body>
<div class="container">
    <div class="card">
        <div class="card-body">
            <form action="/" method="post" class="form-inline">
                @csrf

                <select name="brand" class="custom-select">
                    <option selected>Brand</option>

                    @foreach($brands as $brand)
                        <option value="{{ $brand->id }}"
                                @if (session('user.brand') == $brand->id) selected @endif > {{ $brand->brand }}</option>
                    @endforeach
                </select>

                <select name="country" class="custom-select">
                    <option selected>Country</option>

                    @foreach($countries as $country)
                        <option value="{{ $country->code }}"
                                @if (session('user.country') == $country->code) selected @endif>{{ $country->country }}
                        </option>
                    @endforeach
                </select>

                <input type="submit" value="Save params"/>
            </form>
        </div>
    </div>
</div>

<div class="container _container-fluid">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a href="{{ route('index') }}" class="navbar-brand">Games</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a href="{{ route('index') }}" class="nav-item nav-link _active">Home <span
                        class="sr-only">(current)</span></a>
                <a href="{{ route('games.index') }}" class="nav-item nav-link">Games</a>
            </div>
        </div>

        <div class="btn-group float-right" role="group" aria-label="Basic example">
            <div class="dropdown">
                <button
                    class="btn btn-secondary dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    Category
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($categories as $category)
                        <a href="{{ route('categories.show', ['category' => $category->category]) }}"
                           class="dropdown-item">{{ $category->name }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </nav>

    <div class="row">
        <div class="col-md-12 content" style="padding-bottom: 20px;">
            @yield('content')
        </div>
    </div>

    <div class="navbar bottom">
        <div class="col-md-12">
            <p>Copyright {{ date('Y') }} by Michael Dymecki.</p>
        </div>
    </div>
</div>
</body>
</html>
